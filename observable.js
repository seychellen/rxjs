import {Observable} from "rxjs";

const observable = new Observable( observer => {
    observer.next('hello')
    observer.next('world')
})

observable.subscribe(val => console.log(val))